# MSDS 692 Powering Progress: Optimizing Energy Efficiency for Natural Gas-Powered GPUs
### Data Engineering Path
Justin Meyer

[jmeyer@crusoeenergy.com](mailto:jmeyer@crusoeenergy.com)

[jmeyer006@regis.edu](mailto:jmeyer006@regis.edu)



## Crusoe Background

Crusoe Energy is a crypto startup that specializes in using the waste gas generated oil well
production to power crypto mining operations. Methane is 80x more effective as a greenhouse
gas than CO2, and thus the practice of flaring seemingly unusable gas is mandated by the EPA.
Instead of being flared, Crusoe uses this methane to power generators. This is a process they
call digital flare mitigation (DFM), which more completely combusts the methane compared to
a flare stack. The goal of the company is to reduce the need for traditional energy sources and
make crypto mining more sustainable and environmentally friendly.

Crusoe is in the process of expanding operations into a GPU cloud compute offering. Instead of
only powering ASIC crypto miners, Crusoe has begun installing mobile data units (MDU) to
remote locations in North Dakota. The GPU servers inside these mobile units may have a fixed
power capacity at full load, but it's not entirely clear how their heating needs will change in
different climates. These mobile units are equipped with air chillers, which must be configured
to maintain the ideal temperature range within the unit. However, the harsh weather
conditions in these remote locations, such as extremely hot summers and cold winters, can
greatly affect the power draw and cooling system performance. Therefore, gaining insight from
data center telemetry is essential for ensuring the success and efficiency of these mobile data
centers.

## Data Collection

Acquiring the necessary data for analysis proved to be a complex undertaking. 
Simply exporting data was not enough, as a real-time data stream was required for timely alerts (in the future). 
A significant amount of effort was expended in this project to gain access to a stream of realtime data for immediate action, such as an alarm, and for long-term analysis.

The final list of data sources for this dashboard are the following:

1. Zabbix Export to Prometheus for ATS
1. Zabbix Export to Prometheus for UPS
1. Local Airport
1. Zabbix Plugin (exploration only)

## Tools and Technologies

The software utilized in this project is entirely open source, meaning users can review the source code of the services utilized, however access to my companys Zabbix data will not be available publicly.  
Services run inside a docker container providing resource safetey and separation from other processes on a workstation or server.
This design decision enables the software to be easily launched without traditional software installation concerns on either a workstation or a server.

The code contained within this repository outlines the necessary configuration settings for the docker services to operate correctly.

This project's technology stack is designed to maximize accessibility and flexibility while minimizing deployment and maintenance overheads and concerns.

The following Docker containers are deployed in a VPC, and are not accessible to the public internet. Gsithub projects linked:

1. [mybook/zabbix-exporter](https://github.com/MyBook/zabbix-exporter)
1. [prom/prometheus](https://github.com/prometheus)
1. [grafana/grafana-oss](https://github.com/grafana/grafana)

## Installation Notes

See: [scripts/README.md](scripts/README.md) for backend service installation.

## Grafana and Visualization

Grafana is a powerful and widely used visualization tool that allows users to create and share interactive, real-time dashboards. 
One of the primary benefits of Grafana is its flexibility and extensibility. 
It supports a wide range of data sources, including databases, time-series databases, and a vibrant plugin environment, making it easy to visualize data from a variety of sources in one centralized location.

The screenshots of the two Grafana dashboards stored in this repository are presented below. 
To utilize these dashboards, users must configure and run Grafana, create the necessary data sources, and import the dashboards.

*Example Data Source*

![config](image_assets/datasource.png)

*Zabbix Dashboard for Exploration*

This data is only present for 5 days.  The ephemeral nature and poor performance of Zabbix lead to Prometheus as a data achive.

![zabbix dash](image_assets/zabbix.png)

*Prometheus Dashboard*

![prometheus dash](image_assets/prometheusdash.png)

This dash will update itself every minute and is a true real-time data source.

## Data Observations

Although you may observe temperature values that fall below zero, these data points are intentionally left unscrubbed as they represent an identifiable sensor failure. 
No effort has been made to remove these data points from the visualization.  It is also possible to "smooth" curves using PromQL (the prometheus query language).

Some of this reasoning is described in further detail in my weekly progress updates linked below:
1. [week 1: proposal](weekly_reports/week1_proposal.pdf)
1. [week 2: proposal update](weekly_reports/week2_update.pdf)
1. [week 3](weekly_reports/week3_update.pdf)
1. [week 4](weekly_reports/week4_update.pdf)
1. [week 5](weekly_reports/week5_update.pdf)
1. [week 6](weekly_reports/week6_update.pdf)

While developing this project, I encountered a minimum of three unexpected incidents with the HVAC on location. 
By prioritizing the data stream's monitoring, I was able to identify these incidents far earlier than would have been possible otherwise. 
This underscores the project's value to my stakeholders and provides compelling evidence to support further exploration in the upcoming practicum class.

Although not within the scope of this project, there is significant potential for data analysis and predictive modeling to identify the conditions that lead to unexpected states in the cooling system. 
Utilizing mathematical tools within Prometheus, such as linear regression and moving averages, can allow for better situational knowledge and smart alerting.

I wish to continue on this journey in the next semester of MSDS696.