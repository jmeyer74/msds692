#!/bin/bash
# the following services need to be running (basically in this order) to stand up this environment
# see ../README.md

# expected evironment vars
# export ZABBIX_LOGIN=somelogin
# export ZABBIX_PASSWORD=somepass

# default grafana credentials are `admin` / `admin`

#zabbix exporter
docker run -d -it -v $(pwd)/exporter/config.yml:/zabbix_exporter/zabbix_exporter.yml -p 9224:9224 --env=ZABBIX_URL="http://100.69.139.73:8081" --env="ZABBIX_LOGIN=$ZABBIX_LOGIN" --env="ZABBIX_PASSWORD=$ZABBIX_PASSWORD" mybook/zabbix-exporter

#prometheus: time series data server / aggregator
docker run -d -v $(pwd)/prometheus/volume:/prometheus -v $(pwd)/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml -p 9090:9090 prom/prometheus

#grafana: visualization
docker run -d -p 3000:3000 -v $(pwd)/grafana/volume:/var/lib/grafana grafana/grafana-oss:latest
