# Backend Installation and Execution

This document describes the files needed to run the backend code.
Frontend code and grafana configuration are described in the root [README](../README.md) file. 

## Directories and Config Files

All services in this project were tested locally using podman on OSX, and deplyoed in a Docker-CE environment. 

>Note: podman runs a virtual machine on the host OS that gets a root path different from the root path defautled in Docker-CE.
This can affect the volume shares in the startup.sh file.

Both .yml files below are shared into the docker containers via a volume mount.  See the startup.sh file for a proper example.


### ./exporter/config.yml

This is the config file for the Zabbix exporter for prometheus.  The Docker code for this project can be found here:

https://github.com/MyBook/zabbix-exporter

In this config file is the "key-path" for each of the data points I needed to collect.  
An example of a key path is `key: 'apc.enviro.sensor.wireless.temperature.value.[*]'`.  
This path is heavily dependent on the Zabbix configuration.  
Some of the values exported through this code do not strictly follow a dot-hierarchy, but they are somewhat consistent with a naming convention of device.type.name.  

Below is an example of a complete example of a single metric export:

```
  - key: 'apc.enviro.sensor.wireless.temperature.value.[*]'
    name: 'temperature'
    labels:
      app: $1
```

The text output of the export above is served at a `/metrics` web endpoint and looks like the following text in a browser (http):

```
# HELP temperature Temperature Cold-Rack-03
# TYPE temperature untyped
temperature{app="\"1\"",instance="GPU-01-NetBotz"} 18.3
temperature{app="\"1\"",instance="GPU-Proto-Netbotz"} -214748364.70000002
temperature{app="\"2\"",instance="GPU-01-NetBotz"} 17.900000000000002
temperature{app="\"3\"",instance="GPU-01-NetBotz"} 17.7
temperature{app="\"4\"",instance="GPU-01-NetBotz"} 20.700000000000003
temperature{app="\"5\"",instance="GPU-01-NetBotz"} 25.0
temperature{app="\"6\"",instance="GPU-01-NetBotz"} 16.400000000000002
```

The name `temperature` in the yaml turns into the name of the metric.  
The labels grouped by the `*` added to the metrics in-between the `{}` and are matched with the `$1` above.  

The rest of the Zabbix metrics are converted into prometheus compatible metrics in a similar fashion. 

### ./prometheus/config.yml

This file is a very vanilla scrape config.  There is a hard-coded host name of `jmeyer-misc` that will need to point to the readable address of the exporter.

The scrape frequency was greatly reduced from the default of 30s on my installtion as our Zabbix server was heavily loaded and or under provisioned.




### ./startup.sh

#### Expected ENV Vars

>export ZABBIX_LOGIN=somelogin   
export ZABBIX_PASSWORD=somepass

The three commands needed to run all necessary services are documented in this startup script.  
They can be run manually, or via this script.  

Containers will be daemonized and should restart on host OS restarts if Docker is run properly as a system service.